Description:
    This project is converted to JAVA based on the Lauri Hartikka's sample code in JavaScript 


Reference:
    Youtube: What is Blockchain --by Shai Rubin, zlotolow (Youtube account name)
        https://www.youtube.com/watch?v=93E_GzvpMA0&feature=share

    Medium: A blockchain in 200 lines of code --by Lauri Hartikka
        https://medium.com/@lhartikk/a-blockchain-in-200-lines-of-code-963cc1cc0e54

    GitHub repository sample code (Javascript) --by Lauri Hartikka
        https://github.com/lhartikk/naivechain/blob/master/main.js

Simply describe BlockChain:
    BlockChain is a shared or decentralized database maintained by several nodes.
    Every node within the network has a copy of the transactions (ledger), this kind of mechanism is called open ledger.
    The ledger may not be synchronized. In order to resolve the conflict, we will choose the longer chain instead.
    The transaction is undeniable, as the neighbors nodes (Miners) can perform verification on it.
