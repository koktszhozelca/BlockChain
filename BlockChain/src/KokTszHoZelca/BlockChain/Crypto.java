package KokTszHoZelca.BlockChain;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

public class Crypto {

    public static byte[] calculateHash(int index, byte[] prevHash, Timestamp timestamp, byte[] data) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(getCombinedByte(index, prevHash, timestamp, data));
    }

    public static byte[] calculateHashForBlock(Block block) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(getCombinedByte(block.getIndex(), block.getPrevHash(), block.getTimestamp(), block.getData()));
    }

    private static byte[] getCombinedByte(int index, byte[] prevHash, Timestamp timestamp, byte[] data) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(String.valueOf(index).getBytes());
        baos.write(prevHash);
        baos.write(String.valueOf(timestamp.getTime()).getBytes());
        baos.write(data);
        return baos.toByteArray();
    }

    public static void printByte(byte[] data) {
        for (byte b : data) System.out.print(b);
        System.out.println();
    }
}
