package KokTszHoZelca.BlockChain;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

/*
    Reference:
        Youtube: What is Blockchain --by Shai Rubin, zlotolow (Youtube account name)
            https://www.youtube.com/watch?v=93E_GzvpMA0&feature=share
        Medium: A blockchain in 200 lines of code --by Lauri Hartikka
            https://medium.com/@lhartikk/a-blockchain-in-200-lines-of-code-963cc1cc0e54
        GitHub repository sample code (Javascript) --by Lauri Hartikka
            https://github.com/lhartikk/naivechain/blob/master/main.js

    Simply describe BlockChain.
        BlockChain is a shared or decentralized database maintained by several nodes.
        Every node within the network has a copy of the transactions (ledger), this kind of mechanism is called open ledger.
        The ledger may not be synchronized. In order to resolve the conflict, we will choose the longer chain instead.
        The transaction is undeniable, as the neighbors nodes (Miners) can perform verification on it.
*/

public class Block implements Serializable {
    private int index;
    private byte[] prevHash;
    private Timestamp timestamp;
    private byte[] data;
    private byte[] hash;
    private static ArrayList<Block> chain;

    public static void main(String[] args) {
        try {
            //Create two BlockChain and use chainConflictHandler to resolve the issue.
            //The chain after traceBlockChain should be same as bcB.

//            Block bcA = Block.getLastBlock();
//            bcA = bcA.addBlock(bcA.generateNextBlock("Data A".getBytes()));
//            bcA = bcA.addBlock(bcA.generateNextBlock("Data B".getBytes()));
//            bcA.traceBlockChain("bcA's Chain");
//
//            As the communication part is missing, so I assume there is a new outstanding transaction.
//            Block bcB = KokTszHoZelca.BlockChain.Block.getLastBlock();
//            bcB = bcB.addBlock(bcB.generateNextBlock("Data C".getBytes()));
//            bcB.traceBlockChain("bcB's Chain");

//            //Handle the conflict
//            bcA.chainConflictHandler(bcB);
//            bcA.traceBlockChain("bcA's Chain");

            //Create a BlockChain with two elements.
            Block genesisBlk = Block.getLastBlock();
            genesisBlk.addBlock(genesisBlk.generateNextBlock("This is the next block".getBytes()));
            genesisBlk.traceBlockChain("Demo Chain");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public byte[] getSerialized() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(this);
        return out.toByteArray();
    }

    /*
        The first block in BlockChain is called the genesis block
    */
    private static Block createGenesisBlock() {
        try {
            int index = 0;
            byte[] prevHash = "0".getBytes();
            Timestamp timestamp = new Timestamp(0);
            byte[] data = "The genesis block".getBytes();
            byte[] hash = Crypto.calculateHash(index, prevHash, timestamp, data);
            return new Block(index, prevHash, timestamp, data, hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void initBlockChain() {
        if (Block.chain == null) Block.chain = new ArrayList<>();
        if (Block.chain.size() == 0) Block.chain.add(createGenesisBlock());
    }

    private Block(int index, byte[] prevHash, Timestamp timestamp, byte[] data, byte[] hash) {
        this.index = index;
        this.prevHash = prevHash;
        this.timestamp = timestamp;
        this.data = data;
        this.hash = hash;
    }

    public void traceBlockChain(String label) {
        System.out.println("Label: " + label);
        for (Block block : Block.chain) System.out.println(block);
        System.out.println("NULL\n");
    }

    public Block generateNextBlock(byte[] data) throws IOException, NoSuchAlgorithmException {
        int nextIndex = this.index + 1;
        Timestamp nextTimeStamp = new Timestamp(System.currentTimeMillis());
        byte[] hash = Crypto.calculateHash(nextIndex, this.getHash(), nextTimeStamp, data);
        return new Block(nextIndex, this.getHash(), nextTimeStamp, data, hash);
    }

    public static boolean isValid(Block newBlock, Block prevBlock) throws IOException, NoSuchAlgorithmException {
        if (prevBlock.index + 1 != newBlock.index) return false;
        if (!Arrays.equals(prevBlock.getHash(), newBlock.getPrevHash())) return false;
        if (!Arrays.equals(Crypto.calculateHashForBlock(newBlock), newBlock.getHash())) return false;
        return true;
    }

    /*
        Handle the conflict between copies.
        The article stated that the copy with longer chain will be chosen.
     */
    public void chainConflictHandler(Block newBlock) {
        try {
            if (isValidChain(newBlock) && newBlock.getChain().size() > this.chain.size()) {
                Block.chain.clear();
                Block.chain = (ArrayList<Block>) newBlock.getChain().clone();
                //broadcast(getLatestMsg());  //Tell others that the latest chain.
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Block addBlock(Block newBlock) {
        try {
            if (isValid(newBlock, getLastBlock())) Block.chain.add(newBlock);
            else System.out.println("Not valid");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } finally {
            return getLastBlock();
        }
    }

    public static boolean isValidChain(Block block) throws IOException, NoSuchAlgorithmException {
        //If the genesis block is not match, which means that the BlockChain is not created by us.
        if (!block.getChain().get(0).equals(createGenesisBlock())) return false;

        //Check the previous hash one by one.
        Block[] blockChain = block.getChain().toArray(new Block[0]);
        for (int i = 1; i <= blockChain.length - 1; i++)
            if (!isValid(blockChain[i], blockChain[i - 1])) return false;
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Block block = (Block) o;

        if (getIndex() != block.getIndex()) return false;
        if (!Arrays.equals(getPrevHash(), block.getPrevHash())) return false;
        if (!getTimestamp().equals(block.getTimestamp())) return false;
        if (!Arrays.equals(getData(), block.getData())) return false;
        if (!Arrays.equals(getHash(), block.getHash())) return false;
        return getChain().equals(block.getChain());
    }

    public static Block getLastBlock() {
        initBlockChain();
        return Block.chain.get(Block.chain.size() - 1);
    }

    public ArrayList<Block> getChain() {
        return Block.chain;
    }

    public void setChain(ArrayList<Block> chain) {
        this.chain = chain;
    }

    public byte[] getPrevHash() {
        return prevHash;
    }

    public void setPrevHash(byte[] prevHash) {
        this.prevHash = prevHash;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return "Block {\n" +
                "\tindex=" + index + "\n" +
                "\tprevHash=" + Arrays.toString(prevHash) + "\n" +
                "\ttimestamp=" + timestamp.getTime() + "\n" +
                "\tdata=\"" + new String(data) + "\"" + "\n" +
                "\thash=" + Arrays.toString(hash) + "\n" +
                "} --> ";
    }
}
